import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Screen extends JFrame {

    private JPanel leftPanel;
    private JPanel rightPanel;
    private JPanel topPanel;
    private JList listaContacte;
    private JButton saveNewButton;
    private JButton saveExistingButton;
    private JTextField textName;
    private JTextField textEmail;
    private JTextField textPhoneNumber;
    private JTextField textDOB;
    private JLabel labelAge;
    private JPanel buttonNew;
    private JPanel panelMain;
    private JLabel labelName;
    private JLabel labelEmail;
    private JLabel labelPhoneNumber;
    private JLabel labelDOB;
    private JLabel labelAddress;
    private JTextField textAddress;
    private ArrayList<Person> people;
    private DefaultListModel listPeopleModel;

    Screen() {
        super("My fancy contacts");
        this.setContentPane(this.panelMain);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        people = new ArrayList<Person>();
        listPeopleModel = new DefaultListModel();
        listaContacte.setModel(listPeopleModel);
        saveNewButton.setEnabled(false);

        saveNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int personNumber = listaContacte.getSelectedIndex();
                if (personNumber >= 0) {
                    Person p = people.get(personNumber);
                    p.setName(textName.getText());
                    p.setEmail(textEmail.getText());
                    p.setPhoneNumber(textPhoneNumber.getText());
                    p.setAddress(textAddress.getText());
                    p.setDateOfBirth(textDOB.getText());
                    refreshPeopleList();
                }
            }
        });

        saveExistingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Person p = new Person(
                        textName.getText(),
                        textEmail.getText(),
                        textPhoneNumber.getText(),
                        textAddress.getText(),
                        textDOB.getText()

                );
                people.add(p);
                refreshPeopleList();

            }
        });

        listaContacte.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                int personNumber = listaContacte.getSelectedIndex();
                if (personNumber >= 0) {
                    Person p = people.get(personNumber);
                    textName.setText(p.getName());
                    textEmail.setText(p.getEmail());
                    textPhoneNumber.setText(p.getPhoneNumber());
                    textAddress.setText(p.getAddress());
                    textDOB.setText(p.getDateOfBirth().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                    labelAge.setText(Integer.toString(p.getAge()) + " years");
                    saveNewButton.setEnabled(true);
                } else {
                    saveNewButton.setEnabled(false);
                }
            }
        });


    }

    public void refreshPeopleList() {
        listPeopleModel.removeAllElements();
        System.out.println("Removing all pleople from list");
        for (Person p : people) {
            System.out.println("Adding person to list: " + p.getName());
            listPeopleModel.addElement(p.getName());
        }
    }

    public void addPerson(Person p) {
        people.add(p);
        refreshPeopleList();
    }

    public static void main(String[] args) {
        Screen screen = new Screen();
        screen.setVisible(true);

        Person vali = new Person("Valentin Serban", "val3nt1n@tutanota.com", "0755585514", "Braila", "18/11/1987");
        Person aura = new Person("Aura Harsu", "harsuaura@yahoo.com", "0755585514", "Braila", "01/09/1994");

        screen.addPerson(vali);
        screen.addPerson(aura);
    }
}
